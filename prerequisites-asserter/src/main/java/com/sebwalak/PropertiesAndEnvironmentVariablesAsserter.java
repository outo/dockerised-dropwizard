package com.sebwalak;

import java.util.List;
import java.util.Set;

import static java.lang.System.getProperties;
import static java.lang.System.getenv;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;

public class PropertiesAndEnvironmentVariablesAsserter {

    public static void assertPropertiesAndEnvironmentVariablesExist(List<String> expectedEnvVarsKeys, List<String> expectedPropertiesKeys) {
        final Set<String> actualEnvVarsKeys = getenv().keySet();

        final List<String> messages = expectedEnvVarsKeys.stream()
                .filter(s -> !actualEnvVarsKeys.contains(s))
                .map(s -> "environment variable " + s)
                .collect(toList());

        final Set<String> actualPropertiesKeys = getProperties().stringPropertyNames();

        messages.addAll(expectedPropertiesKeys.stream()
                .filter(s -> !actualPropertiesKeys.contains(s))
                .map(s -> "property " + s)
                .collect(toList()));

        assertThat("Missing environment variables and/or properties", messages, empty());
    }
}