package com.sebwalak;

import io.restassured.http.ContentType;
import org.dbunit.DBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.sebwalak.PropertiesAndEnvironmentVariablesAsserter.assertPropertiesAndEnvironmentVariablesExist;
import static io.restassured.RestAssured.given;
import static java.lang.System.getProperty;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.text.IsEmptyString.isEmptyString;

public class SkimTestIT extends DBTestCase {

    static {
        assertPropertiesAndEnvironmentVariablesExist(
                emptyList(),
                asList(
                        "dbunit.connectionUrl",
                        "dbunit.driverClass",
                        "dbunit.username",
                        "dbunit.password",
                        "userService.url"
                ));
    }

    private String userServiceUrl;

    @Override
    protected IDataSet getDataSet() throws Exception {
        final FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
        flatXmlDataSetBuilder.setDtdMetadata(false);
        return flatXmlDataSetBuilder.build(ClassLoader.getSystemResourceAsStream("dataset.xml"));
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        userServiceUrl = getProperty("userService.url");
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testShouldBeAbleToAddNewUserToEmptyDatabase() {

        // @formatter:on

        given().
                body("{\n" +
                        "  \"username\": \"TestUser6\",\n" +
                        "  \"firstname\": \"John\",\n" +
                        "  \"lastname\": \"Doe\",\n" +
                        "  \"age\": \"25\"\n" +
                        "}").with().contentType(ContentType.JSON).then().
                expect().
                statusCode(200).and().body(isEmptyString()).
                when().
                post(userServiceUrl + "/add");


        // @formatter:off
    }

    @Test
    public void testShouldNotBeAbleToAddUserWithIdenticalUsername() {
        // @formatter:off

        given().
                body("{\n" +
                        "  \"username\": \"TestUser6\",\n" +
                        "  \"firstname\": \"John\",\n" +
                        "  \"lastname\": \"Doe\",\n" +
                        "  \"age\": \"25\"\n" +
                        "}").with().contentType(ContentType.JSON).then().
                expect().
                statusCode(200).and().body(equalTo("")).
                when().
                post(userServiceUrl + "/add");

        given().
                body("{\n" +
                        "  \"username\": \"TestUser6\",\n" +
                        "  \"firstname\": \"Hannah\",\n" +
                        "  \"lastname\": \"Smith\",\n" +
                        "  \"age\": \"39\"\n" +
                        "}").with().contentType(ContentType.JSON).then().
                expect().
                statusCode(500).
                when().
                post(userServiceUrl + "/add");

        // @formatter:on
    }

    @Test
    public void testShouldNotBeAbleToAddUserWithMalformedPayload() {

        // @formatter:on

        given().
                body("{ not_even: correct json}").with().contentType(ContentType.JSON).then().
                expect().
                statusCode(400).and().
                body(hasJsonPath("$.code", equalTo(400))).and().
                body(hasJsonPath("$.message", equalTo("Payload is not valid"))).
                when().
                post(userServiceUrl + "/add");

        // @formatter:off
    }

    @Test
    public void testShouldNotBeAbleToAddUserWithWellFormedButInvalidData() {

        // @formatter:on

        given().
                body("{\n" +
                        "  \"username\": \"Test User6\",\n" +
                        "  \"firstname\": \"John\",\n" +
                        "  \"lastname\": \"Doe\",\n" +
                        "  \"age\": \"200\"\n" +
                        "}").with().contentType(ContentType.JSON).then().
                expect().
                statusCode(400).and().
                body(hasJsonPath("$.code", equalTo(400))).and().
                body(hasJsonPath("$.message", containsString("username must match"))).and().
                body(hasJsonPath("$.message"), containsString("age must be less than or equal")).
                when().
                post(userServiceUrl + "/add");

        // @formatter:off
    }

}
