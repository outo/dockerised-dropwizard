# What is it?

It started as an exercise that was supposed to satisfy few, artificial requirements for a technical interview.  

I kept building on it to get more practise but also to provide some framework for my future work. 

It incorporates few techniques and technologies and should be useful for projects using:
- Maven to orchestrate the following:
  - build code
  - package
  - unit test
  - build Docker images
  - execute integration tests against Docker container
  - execute e2e tests against all containers (just a demo)
  - push Docker images to registry  
  Simple dependency mechanism is used to chain the above steps. 
  It increases number of modules but makes each module manageable and enables concurrent execution.
- Docker (building and pushing images, running containers for testing purpose)
- DropWizard
- Liquibase
- externalisation of sensitive details into files

The functional aspect of the code itself is not that relevant as the project became just an experimentation ground.   
Originally it was supposed to be a REST API that allows for creating user and storing its details in MySQL database.   

# General prerequisites

The following steps are not specific to this project and you may already have all of these in working order. 

## On Windows use Bash command-line interpreter 
I'm using MINGW as it was readily available as part of Git for Windows installation. 
Few scripts and commands are leveraging commands that are normally unavailable in Windows Command Prompt.  

## Docker machine

If using Windows without support for native linux containers (here Windows 7)

Install Docker Toolbox (not Docker for Windows) from <https://docs.docker.com/toolbox/toolbox_install_windows/>.  
I have found it's best to first uninstall VirtualBox (if that's) your choice of virtualisation platform.  
Once that's done install Docker Toolbox with the optional VirtualBox package selected.  
Trying to reuse existing installation of VirtualBox worked for me but was incredibly slow (I haven't figured out what
went wrong yet).  
If you want to try reusing existing VirtualBox installation, create virtual machine with:  
`docker-machine create --driver virtualbox --virtualbox-cpu-count "2" --virtualbox-memory "8000" --virtualbox-disk-size "20000" default`  
Docker Toolbox contains docker and docker-compose cmd-line tools. 

If you are running under Linux host, then you only need to install docker-ce and docker-compose.  
For Ubuntu look at https://docs.docker.com/install/linux/docker-ce/ubuntu/ and
https://docs.docker.com/compose/install/
 
If you are running on a Windows OS host with support for native linux containers install Docker for Windows (<https://docs.docker.com/docker-for-windows/install/>).  
More info about this feature <https://docs.microsoft.com/en-us/virtualization/windowscontainers/deploy-containers/linux-containers>. 

## Docker Registry 

**You can use Docker hub or local registry, amongst other options. These steps are for AWS ECR.**

###1. Install AWS-CLI if you didn't already
It will be used to configure credentials for AWS services and will generate `docker login` command. 
This command will enable your docker instance to authenticate within ECR.  
AWS-CLI download and installation instructions at <https://aws.amazon.com/cli>

###2. Create AWS user

Go to AWS IAM service at <https://aws.amazon.com/iam> and create a new user.
For this activity you should only need programmatic access (AWS console access optional).  
You also need to add permissions described by policy `AmazonEC2ContainerRegistryFullAccess` (within IAM).    
You can fine-tune the policy details to make it more tight. 
Make a note of your AWS Access Key ID and AWS Secret Access Key.  

###3. Configure credentials for AWS
If you run the following command you will be asked for your AWS Access Key ID, AWS Secret Access Key and default region. 
You should have all of these available from previous steps.  
`aws configure`

###4. Verify you can authenticate docker with AWS ECR  
`$(aws ecr get-login --no-include-email --region=<aws_ecr_region>)`


# Initial, one-time, project specific setup

## Clone the code

`git clone git@bitbucket.org:outo/dockerised-dropwizard.git`

## Docker Registry

###1. Create empty ECR registries.  
Go to https://aws.amazon.com/ecr/ and log into your AWS account.
Select region where the registry is going to sit.  
- dockerised-dropwizard/mysql-migrated
- dockerised-dropwizard/user-service

###2. Provide domain of Registry URI in project's config file  
Make a note of the Repository URI shown on the top of two newly created repositories.  
You need to provide registry's domain (same for both repositories) in 
`<project_top>/config/sensitive/tooling.properties`.  
It is done under key, format for reference
`DOCKER_REGISTRY=<aws_acc_no>.dkr.ecr.<aws_ecr_region>.amazonaws.com`

###3. Provide configuration for mysql docker container 
In `config/sensitive/container.db.properties` supply:
```
DB_ROOT_PASSWORD=<db root password>
DB_USERSERVICE_USERNAME=<username for userservice db user>
DB_USERSERVICE_PASSWORD=<password for userservice db user>
DB_USERSERVICE_NAME=<database name for userservice>
```

###4. Provide configuration for userservice REST endpoint:
In `config/sensitive/container.userservice.properties` supply:
```
USERSERVICE_DB_USERNAME=<username for userservice db user>
USERSERVICE_DB_PASSWORD=<password for userservice db user>
USERSERVICE_DB_NAME=<database name for userservice>
USERSERVICE_APP_PORT=8080
USERSERVICE_ADMIN_PORT=9080
```

# Repetitive steps

##1. On Windows (without native linux support) start docker machine
For docker commands to work start docker machine if it is not running (`docker-machine ls` to check) 
`docker-machine start`

Also, few environment variables need to be populated, all done with:  
`eval $(docker-machine env)`

On Linux host this step is not needed. 

##2. Docker has to be logged into registry (here ECR)
`$(aws ecr get-login --no-include-email --region=<aws_ecr_region>)`

This step needs to be done for any host OS.

`aws ecr get-login` will generate `docker login` command which we just evaluate here. 
  
Session lasts few hours.  

##3. Managing the version of the maven artifacts/docker images
There is no automatic mechanism in place [yet] to auto-increment the version.   
Manually it can be done with:  
`mvn versions:set -DnewVersion=1.0.5`

##4. Building the project 

To build, run unit and integration tests execute:  
`./mvnw clean install` 

This will also build docker images and store them locally.  
Run `docker images` to inspect. 

**Note**:  
Don't use maven command directly (`mvn`) on your machine as the version may not be what's required.   
For example the top pom file requires "maven.multiModuleProjectDirector" property which has been introduced in version 3.3.1.   
Maven wrapper (`<project_top>/mvnw`)ensures the correct maven version is used for the build and the results are repeatable.  

If you also want to push the images to ECR (conditional on all tests passing) run:  
`./mvnw clean install -DdockerPush`  

##5. Running
    
If you want to spin all containers locally for debugging/experimentation purposes run:  
`./docker-compose-up.sh`

To run code directly from IDE against some of the containers run   
`./docker-compose-up.sh db`
which will only spin up mysql container

Additionally, when running against docker machine, for the above case to work you also have to provide docker machine ip output for `dockerHostIp` property in top-level pom. 
On Linux it can be left as default (localhost) if default network setup is used. 



