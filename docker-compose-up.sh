#!/usr/bin/env bash

# merge all properties file into .env file
awk '/.*=.*/{print $0}' ./config/sensitive/*.properties > .env
docker-compose up -d "$@"

# remove properties' values (but keep the keys to stop "docker-compose logs" from complaining)
awk -F'=' '/.*=.*/{print $1 "="}' ./config/sensitive/*.properties > .env
docker-compose logs -f

