package com.sebwalak.core;

import com.sebwalak.api.User;
import com.sebwalak.db.UserDAO;

public class UserService {

    private UserDAO userDAO;

    public UserService(final UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void addUser(final User user) {
        userDAO.insert(user.getUsername(), user.getFirstname(), user.getLastname(), user.getAge());
    }
}
