package com.sebwalak;

import com.sebwalak.db.UserRowMapper;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import lombok.Builder;
import lombok.NonNull;
import org.jdbi.v3.core.Jdbi;

@Builder
public class DockerisedDropWizardJdbi {

    @NonNull
    private Environment environment;

    @NonNull
    private DataSourceFactory dataSourceFactory;

    @Builder.Default private String poolName = "database";

    public DockerisedDropWizardJdbi(final Environment environment, final DataSourceFactory dataSourceFactory, final String poolName) {
        this.environment = environment;
        this.dataSourceFactory = dataSourceFactory;
        this.poolName = poolName;
    }

    public Jdbi getJdbi() {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, dataSourceFactory, poolName);
        jdbi.registerRowMapper(new UserRowMapper());
        return jdbi;
    }
}
