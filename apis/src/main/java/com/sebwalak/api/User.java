package com.sebwalak.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {

    @NotNull
    @Pattern(regexp = "\\w{1,50}")
    @EqualsAndHashCode.Include
    private String username;

    @NotBlank
    @Length(min = 1, max = 50)
    private String firstname;

    @NotBlank
    @Length(min = 1, max = 50)
    private String lastname;

    @NotNull
    @Min(0)
    @Max(130)
    private Integer age;

}
