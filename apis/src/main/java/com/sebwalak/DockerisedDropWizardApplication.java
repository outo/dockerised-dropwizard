package com.sebwalak;

import com.sebwalak.core.UserService;
import com.sebwalak.db.UserDAO;
import com.sebwalak.providers.JsonProcessingExceptionMapper;
import com.sebwalak.providers.RuntimeExceptionMapper;
import com.sebwalak.providers.ViolationExceptionMapper;
import com.sebwalak.resources.UserResource;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.jdbi.v3.core.Jdbi;

public class DockerisedDropWizardApplication extends Application<DockerisedDropWizardConfiguration> {

    public static void main(final String[] args) throws Exception {
        new DockerisedDropWizardApplication().run(args);
    }

    @Override
    public String getName() {
        return "DockerisedDropWizard";
    }

    @Override
    public void initialize(Bootstrap<DockerisedDropWizardConfiguration> bootstrap) {
        // Enable variable substitution with environment variables
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(true)
                )
        );

    }

    @Override
    public void run(final DockerisedDropWizardConfiguration configuration, final Environment environment) {
        final Jdbi jdbi = DockerisedDropWizardJdbi.builder().
                dataSourceFactory(configuration.getDataSourceFactory()).
                environment(environment).
                build().
                getJdbi();

        final UserDAO userDAO = jdbi.onDemand(UserDAO.class);
        final UserService userService = new UserService(userDAO);
        environment.jersey().register(new UserResource(userService));

        environment.jersey().register(RuntimeExceptionMapper.class);
        environment.jersey().register(ViolationExceptionMapper.class);
        environment.jersey().register(JsonProcessingExceptionMapper.class);
    }

}
