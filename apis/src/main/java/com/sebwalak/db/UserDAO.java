package com.sebwalak.db;

import com.sebwalak.api.User;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface UserDAO {

    @SqlUpdate("insert into app_users (username, firstname, lastname, age) values (:username, :firstname, :lastname, :age)")
    void insert(@Bind("username") String username, @Bind("firstname") String firstname, @Bind("lastname") String lastname, @Bind("age") int age);

    @SqlQuery("select username, firstname, lastname, age from app_users")
    List<User> findAll();
}

