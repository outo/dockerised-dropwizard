package com.sebwalak.db;

import com.sebwalak.api.User;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {

    @Override
    public User map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new User(
                rs.getString("username"),
                rs.getString("firstname"),
                rs.getString("lastname"),
                rs.getInt("age")
        );
    }
}
