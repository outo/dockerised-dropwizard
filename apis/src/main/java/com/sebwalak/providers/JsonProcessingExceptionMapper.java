package com.sebwalak.providers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.jaxrs.base.JsonParseExceptionMapper;
import io.dropwizard.jersey.errors.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

public class JsonProcessingExceptionMapper implements ExceptionMapper<JsonProcessingException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonParseExceptionMapper.class);

    @Override
    public Response toResponse(final JsonProcessingException exception) {
        LOGGER.error("I have got JsonProcessingExceptionMapper: " + exception.getMessage());

        return Response.status(BAD_REQUEST)
                .entity(new ErrorMessage(BAD_REQUEST.getStatusCode(), "Payload is not valid"))
                .type(APPLICATION_JSON_TYPE)
                .build();
    }
}
