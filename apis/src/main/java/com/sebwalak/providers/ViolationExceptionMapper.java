package com.sebwalak.providers;

import io.dropwizard.jersey.errors.ErrorMessage;
import io.dropwizard.jersey.validation.ConstraintMessage;
import io.dropwizard.jersey.validation.JerseyViolationException;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.server.model.Invocable;

import javax.validation.ConstraintViolation;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Optional;
import java.util.Set;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Provider
public class ViolationExceptionMapper implements ExceptionMapper<JerseyViolationException> {

    @Override
    public Response toResponse(final JerseyViolationException exception) {
        final Set<ConstraintViolation<?>> violations = exception.getConstraintViolations();
        final Invocable invocable = exception.getInvocable();
        int status = ConstraintMessage.determineStatus(violations, invocable);

        if (status == 422) {
            status = BAD_REQUEST.getStatusCode();
        }

        final Optional<String> text = violations.stream()
                .map(constraintViolation -> ConstraintMessage.getMessage(constraintViolation, invocable) + "\n")
                .reduce((s, s2) -> s + s2)
                .map(StringUtils::chomp);

        return Response.status(status)
                .type(APPLICATION_JSON_TYPE)
                .entity(new ErrorMessage(BAD_REQUEST.getStatusCode(), text.orElse("")))
                .build();
    }
}
