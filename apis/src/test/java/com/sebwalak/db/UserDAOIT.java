package com.sebwalak.db;

import com.sebwalak.DockerisedDropWizardApplication;
import com.sebwalak.DockerisedDropWizardConfiguration;
import com.sebwalak.DockerisedDropWizardJdbi;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.dbunit.IDatabaseTester;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.jdbi.v3.core.Jdbi;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import static com.sebwalak.PropertiesAndEnvironmentVariablesAsserter.assertPropertiesAndEnvironmentVariablesExist;
import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;
import static java.util.Arrays.asList;
import static org.dbunit.Assertion.assertEquals;

public class UserDAOIT {

    static {
        assertPropertiesAndEnvironmentVariablesExist(asList(
                "DB_PORT",
                "DB_NAME",
                "DB_USERNAME",
                "DB_PASSWORD",
                "APP_PORT",
                "ADMIN_PORT"
        ), asList(
                "dbunit.connectionUrl",
                "dbunit.driverClass",
                "dbunit.username",
                "dbunit.password"
        ));
    }

    @ClassRule
    public static final DropwizardAppRule<DockerisedDropWizardConfiguration> RULE =
            new DropwizardAppRule<>(DockerisedDropWizardApplication.class,
                    resourceFilePath("config.yml"));


    private UserDAO userDAO;

    private IDatabaseTester databaseTester;

    @Before
    public void setUp() throws Exception {
        databaseTester = new PropertiesBasedJdbcDatabaseTester();
        databaseTester.setDataSet(datasetFromFlatXml("dataset-empty.xml"));

        // will call default setUpOperation
        databaseTester.onSetup();

        final Jdbi jdbi = DockerisedDropWizardJdbi.builder().
                dataSourceFactory(RULE.getConfiguration().getDataSourceFactory()).
                environment(RULE.getEnvironment()).
                build().
                getJdbi();

        userDAO = jdbi.onDemand(UserDAO.class);
    }

    @After
    public void tearDown() throws Exception {
        databaseTester.onTearDown();
    }

    @Test
    public void testDatabaseHasNoUnexpectedRowsOnStart() throws Exception {
        assertEquals(
                datasetFromFlatXml("dataset-empty.xml").getTable("app_users"),
                actualDataset().getTable("app_users"));
    }

    private IDataSet datasetFromFlatXml(final String datasetName) throws DataSetException {
        final FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
        flatXmlDataSetBuilder.setDtdMetadata(false);
        return flatXmlDataSetBuilder.build(ClassLoader.getSystemResourceAsStream(datasetName));
    }

    private IDataSet actualDataset() throws Exception {
        return databaseTester.getConnection().createDataSet();
    }

    @Test
    public void testAddsUser() throws Exception {
        userDAO.insert("testusername", "testfirstname", "testlastname", 32);

        assertEquals(
                datasetFromFlatXml("dataset-adds-user.xml").getTable("app_users"),
                actualDataset().getTable("app_users"));
    }
}