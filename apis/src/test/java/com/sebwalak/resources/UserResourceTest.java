package com.sebwalak.resources;

import com.sebwalak.api.User;
import com.sebwalak.core.UserService;
import com.sebwalak.providers.JsonProcessingExceptionMapper;
import com.sebwalak.providers.RuntimeExceptionMapper;
import com.sebwalak.providers.ViolationExceptionMapper;
import io.dropwizard.jersey.errors.ErrorMessage;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class UserResourceTest {
    private final UserService serviceMock = mock(UserService.class);

    private static final String TEST_USERNAME = "TestUsername";
    private static final String TEST_FIRSTNAME = "TestFirstname";
    private static final String TEST_LASTNAME = "TestLastname";
    private static final Integer TEST_AGE = 72;
    private static final User TEST_USER = new User(TEST_USERNAME, TEST_FIRSTNAME, TEST_LASTNAME, TEST_AGE);

    @Rule
    public final ResourceTestRule resources = ResourceTestRule.builder()
            .addProvider(RuntimeExceptionMapper.class)
            .addProvider(ViolationExceptionMapper.class)
            .addProvider(JsonProcessingExceptionMapper.class)
            .addResource(new UserResource(serviceMock))
            .build();


    @Test
    public void delegatesAddingUserToAService() {
        resources
                .target("/user/add")
                .request(APPLICATION_JSON_TYPE)
                .post(entity(TEST_USER, APPLICATION_JSON_TYPE));

        verify(serviceMock).addUser(TEST_USER);
        verifyNoMoreInteractions(serviceMock);
    }

    @Test
    public void returnsHttp200CodeWhenAddingUserIsSuccessful() {
        final Response actualResponse = sendRequest(TEST_USER);
        assertThat(actualResponse.getStatusInfo(), equalTo(OK));
    }

    @Test
    public void returnsHttp400CodeWithMessageWhenInputValueObjectIsMissing() {
        final Response actualResponse = sendRequest(null);
        assertThat(actualResponse.getStatusInfo(), equalTo(BAD_REQUEST));
        final ErrorMessage actualErrorMessage = actualResponse.readEntity(ErrorMessage.class);
        assertThat(actualErrorMessage.getCode(), equalTo(BAD_REQUEST.getStatusCode()));
        assertThat(actualErrorMessage.getMessage(), containsString("The request body may not be null"));
        assertThat(actualResponse.getHeaderString("Content-Type"), equalTo(APPLICATION_JSON));

    }

    @Test
    public void returnsHttp500CodeWithMessageWhenServerGoesTitsUp() {
        doThrow(new RuntimeException("The server went boom")).when(serviceMock).addUser(any());
        final Response actualResponse = sendRequest(TEST_USER);
        assertThat(actualResponse.getStatusInfo(), equalTo(INTERNAL_SERVER_ERROR));
        assertThat(actualResponse.readEntity(ErrorMessage.class), equalTo(new ErrorMessage("The server went boom")));
        assertThat(actualResponse.getHeaderString("Content-Type"), equalTo(APPLICATION_JSON));
    }

    @Test
    public void returnsHttp400CodeWithMessageWhenPayloadContainsInvalidDataTypes() {
        final Response actualResponse = resources
                .target("/user/add")
                .request(APPLICATION_JSON_TYPE)
                .post(entity(fixture("fixtures/add_user_invalid_data_type.json"), APPLICATION_JSON_TYPE));

        assertThat(actualResponse.getStatusInfo(), equalTo(BAD_REQUEST));
        assertThat(actualResponse.readEntity(ErrorMessage.class),
                equalTo(new ErrorMessage(BAD_REQUEST.getStatusCode(), "Payload is not valid")));
        assertThat(actualResponse.getHeaderString("Content-Type"), equalTo(APPLICATION_JSON));
    }

    @Test
    public void returnsHttp400CodeWithMessageWhenPayloadIsMissingField() {
        final Response actualResponse = resources
                .target("/user/add")
                .request(APPLICATION_JSON_TYPE)
                .post(entity(fixture("fixtures/add_user_invalid_payload.json"), APPLICATION_JSON_TYPE));

        assertThat(actualResponse.getStatusInfo(), equalTo(BAD_REQUEST));
        final ErrorMessage actualErrorMessage = actualResponse.readEntity(ErrorMessage.class);
        assertThat(actualErrorMessage.getCode(), equalTo(BAD_REQUEST.getStatusCode()));
        assertThat(actualErrorMessage.getMessage(), containsString("may not be null"));
        assertThat(actualErrorMessage.getMessage(), containsString("may not be empty"));
        assertThat(actualResponse.getHeaderString("Content-Type"), equalTo(APPLICATION_JSON));
    }

    private Response sendRequest(final User user) {
        return resources
                .target("/user/add")
                .request(APPLICATION_JSON_TYPE)
                .post(entity(user, APPLICATION_JSON_TYPE));
    }
}

