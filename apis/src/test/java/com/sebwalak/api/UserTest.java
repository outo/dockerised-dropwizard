package com.sebwalak.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

public class UserTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();
    private static final String ADD_USER_SUCCESS_JSON_FILENAME = "fixtures/add_user_success.json";
    private static final String TEST_USERNAME = "TestUser";
    private static final String TEST_FIRSTNAME = "John";
    private static final String TEST_LASTNAME = "Doe";
    private static final Integer TEST_AGE = 25;

    @Test
    public void serializesToJSON() throws Exception {
        final User user = new User(TEST_USERNAME, TEST_FIRSTNAME, TEST_LASTNAME, TEST_AGE);

        final String expected =
                MAPPER.writeValueAsString(MAPPER.readValue(fixture(ADD_USER_SUCCESS_JSON_FILENAME), User.class));

        assertThat(MAPPER.writeValueAsString(user)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        final User user = new User(TEST_USERNAME, TEST_FIRSTNAME, TEST_LASTNAME, TEST_AGE);

        String fixture = fixture(ADD_USER_SUCCESS_JSON_FILENAME);
        User deserialisedUser = MAPPER.readValue(fixture, User.class);

        assertThat(deserialisedUser.getUsername()).isEqualTo(user.getUsername());
        assertThat(deserialisedUser.getFirstname()).isEqualTo(user.getFirstname());
        assertThat(deserialisedUser.getLastname()).isEqualTo(user.getLastname());
        assertThat(deserialisedUser.getAge()).isEqualTo(user.getAge());
    }
}