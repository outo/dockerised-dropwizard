package com.sebwalak.api;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import lombok.Getter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(JUnitParamsRunner.class)
public class UserValidationTest {

    private static Validator validator;
    private static final String VALID_USERNAME = "test_Username1";
    private static final String VALID_FIRSTNAME = "test firstname";
    private static final String VALID_LASTNAME = "test lastname";
    private static final int VALID_AGE = 20;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    private TestCase[] validationParameters() {
        return new TestCase[]{
                /* username */
                new HappyTestCase("username can be as long as 50 characters", stringOfNChars(50, 'a'), VALID_FIRSTNAME, VALID_LASTNAME, VALID_AGE),
                new HappyTestCase("username can contain a to Z, 0 to 9 and underscore", "aushjusdSDIFHDS2034732967_", VALID_FIRSTNAME, VALID_LASTNAME, VALID_AGE),
                new TestCase("username is mandatory", null, VALID_FIRSTNAME, VALID_LASTNAME, VALID_AGE, "username:may not be null"),
                new TestCase("username cannot contain spaces", "sd as", VALID_FIRSTNAME, VALID_LASTNAME, VALID_AGE, "username:must match \"\\w{1,50}\""),
                new TestCase("username cannot contain tabs", "fds\tdfs", VALID_FIRSTNAME, VALID_LASTNAME, VALID_AGE, "username:must match \"\\w{1,50}\""),
                new TestCase("username cannot contain newline", "sdfdoj\ndfdf", VALID_FIRSTNAME, VALID_LASTNAME, VALID_AGE, "username:must match \"\\w{1,50}\""),
                new TestCase("username cannot be longer than 50 characters", stringOfNChars(51, 'a'), VALID_FIRSTNAME, VALID_LASTNAME, VALID_AGE, "username:must match \"\\w{1,50}\""),
                /* firstname */
                new HappyTestCase("first name can be as long as 50 characters", VALID_USERNAME, stringOfNChars(50, 'a'), VALID_LASTNAME, VALID_AGE),
                new HappyTestCase("first name can be multipart with diacritics", VALID_USERNAME, "Nguyễn Tấn Dũng", VALID_LASTNAME, VALID_AGE),
                new HappyTestCase("first name can be hyphenated", VALID_USERNAME, "Anne-Laura", VALID_LASTNAME, VALID_AGE),
                new HappyTestCase("first name can be Japanese", VALID_USERNAME, "東海林賢蔵", VALID_LASTNAME, VALID_AGE),
                new HappyTestCase("first name can be Arabic", VALID_USERNAME, "عامر", VALID_LASTNAME, VALID_AGE),
                new TestCase("first name is mandatory", VALID_USERNAME, null, VALID_LASTNAME, VALID_AGE, "firstname:may not be empty"),
                new TestCase("first name cannot be just whitespace", VALID_USERNAME, "   ", VALID_LASTNAME, VALID_AGE, "firstname:may not be empty"),
                new TestCase("first name cannot be longer than 50 characters", VALID_USERNAME, stringOfNChars(51, 'a'), VALID_LASTNAME, VALID_AGE, "firstname:length must be between 1 and 50"),
                /* lastname */
                new HappyTestCase("last name can be as long as 50 characters", VALID_USERNAME, VALID_FIRSTNAME, stringOfNChars(50, 'a'), VALID_AGE),
                new HappyTestCase("last name can be multipart with diacritics", VALID_USERNAME, VALID_FIRSTNAME, "Nguyễn Tấn Dũng", VALID_AGE),
                new HappyTestCase("last name can be double-barrel", VALID_USERNAME, VALID_FIRSTNAME, "Rees-Mogg", VALID_AGE),
                new HappyTestCase("last name can contain an apostrophe", VALID_USERNAME, VALID_FIRSTNAME, "O'Connor", VALID_AGE),
                new HappyTestCase("last name can be Japanese", VALID_USERNAME, VALID_FIRSTNAME, "東海林賢蔵", VALID_AGE),
                new HappyTestCase("last name can be Arabic", VALID_USERNAME, VALID_FIRSTNAME, "عامر", VALID_AGE),
                new TestCase("last name is mandatory", VALID_USERNAME, VALID_FIRSTNAME, null, VALID_AGE, "lastname:may not be empty"),
                new TestCase("last name cannot be just whitespace", VALID_USERNAME, VALID_FIRSTNAME, "   ", VALID_AGE, "lastname:may not be empty"),
                new TestCase("last name cannot be longer than 50 characters", VALID_USERNAME, VALID_FIRSTNAME, stringOfNChars(51, 'a'), VALID_AGE, "lastname:length must be between 1 and 50"),
                /* age */
                new HappyTestCase("age can be 0 (as per questionable requirement)", VALID_USERNAME, VALID_FIRSTNAME, VALID_LASTNAME, 0),
                new HappyTestCase("age can be 130 (as per presumed functional requirement)", VALID_USERNAME, VALID_FIRSTNAME, VALID_LASTNAME, 130),
                new TestCase("age cannot be negative", VALID_USERNAME, VALID_FIRSTNAME, VALID_LASTNAME, -1, "age:must be greater than or equal to 0"),
                new TestCase("age cannot be higher than 130", VALID_USERNAME, VALID_FIRSTNAME, VALID_LASTNAME, 131, "age:must be less than or equal to 130"),
                new TestCase("age is mandatory", VALID_USERNAME, VALID_FIRSTNAME, VALID_LASTNAME, null, "age:may not be null"),
        };
    }

    @Test
    @Parameters(method = "validationParameters")
    public void shouldValidateAppropriately(final TestCase testCase) {
        final User user = new User(testCase.username, testCase.firstname, testCase.lastname, testCase.age);
        final Set<String> actualViolations = validator.validate(user).stream()
                .map(this::violationMapper)
                .collect(toSet());
        assertThat(testCase.reason, actualViolations, equalTo(testCase.expectedViolations));
    }

    /**************************************************************************/

    private String stringOfNChars(final int n, final char ch) {
        return new String(new char[n]).replace("\0", String.valueOf(ch));
    }

    private String violationMapper(ConstraintViolation<User> violation) {
        return violation.getPropertyPath() + ":" + violation.getMessage();
    }

    private class TestCase {
        @Getter
        private String reason;
        @Getter
        private String username;
        @Getter
        private String firstname;
        @Getter
        private String lastname;
        @Getter
        private Integer age;
        @Getter
        private Set<String> expectedViolations;

        private TestCase(final String reason, final String username, final String firstname, final String lastname, final Integer age, final String... violations) {
            this.reason = reason;
            this.username = username;
            this.firstname = firstname;
            this.lastname = lastname;
            this.age = age;
            this.expectedViolations = new HashSet<>(asList(violations));
        }

    }

    private class HappyTestCase extends TestCase {
        private HappyTestCase(String reason, String username, String firstname, String lastname, Integer age) {
            super(reason, username, firstname, lastname, age);
        }

    }

}
